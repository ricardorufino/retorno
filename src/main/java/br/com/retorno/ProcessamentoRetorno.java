/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.retorno;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ricardo
 */
public abstract class ProcessamentoRetorno {

    private final File arquivoRetorno;
    private final List<SegmentoT> segmentos;

    public ProcessamentoRetorno(File arquivoRetorno) {
        this.arquivoRetorno = arquivoRetorno;
        segmentos = new ArrayList<>();
    }

    public abstract void implementacao(BufferedReader br, File arquivoRetorno) throws FileNotFoundException, IOException;

    public void processar() throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(arquivoRetorno));
        implementacao(br, arquivoRetorno);
        br.close();
    }

    public List<SegmentoT> getSegmentos() {
        return segmentos;
    }
}
