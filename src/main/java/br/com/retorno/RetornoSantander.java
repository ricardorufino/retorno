/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.retorno;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author ricardo
 */
public class RetornoSantander extends ProcessamentoRetorno {

    public RetornoSantander(File arquivoRetorno) {
        super(arquivoRetorno);
    }

    @Override
    public void implementacao(BufferedReader br, File arquivoRetorno) throws FileNotFoundException, IOException {
        String str = "";
        while ((str = br.readLine()) != null) {
            if (String.valueOf(str.charAt(13)).equals("T")) {
                SegmentoT st = new SegmentoT(
                        str.substring(0, 3),
                        str.substring(3, 7),
                        str.substring(7, 8),
                        str.substring(8, 13),
                        str.substring(13, 14),
                        str.substring(14, 15),
                        str.substring(15, 17),
                        str.substring(17, 21),
                        str.substring(21, 22),
                        str.substring(22, 31),
                        str.substring(31, 32),
                        str.substring(32, 40),
                        str.substring(42, 52),
                        str.substring(53, 54),
                        str.substring(54, 69),
                        str.substring(69, 77),
                        str.substring(77, 92),
                        str.substring(92, 95),
                        str.substring(95, 99),
                        str.substring(99, 100),
                        str.substring(100, 125),
                        str.substring(125, 127),
                        str.substring(127, 128),
                        str.substring(128, 143),
                        str.substring(143, 183),
                        str.substring(183, 193),
                        str.substring(193, 208),
                        str.substring(208, 218),
                        str.substring(218, 240));
                super.getSegmentos().add(st);
            }
        }
    }

}
