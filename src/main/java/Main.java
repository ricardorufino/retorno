/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ricardo
 */
import br.com.retorno.*;
import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            System.out.println("---- BANCO DO BRASIL ----");
            ProcessamentoRetorno bancoBrasil = new RetornoBancoBrasil(new File("src/main/java/BancoBrasil.RET"));
            bancoBrasil.processar();
            for (SegmentoT segmentoT : bancoBrasil.getSegmentos()) {
                System.out.println(segmentoT.getNossoNumero() + " - " + segmentoT.getValorNominalTitulo());
            }

            System.out.println("---- CAIXA ----");
            ProcessamentoRetorno caixa = new RetornoBancoBrasil(new File("src/main/java/Caixa.RET"));
            caixa.processar();
            for (SegmentoT segmentoT : caixa.getSegmentos()) {
                System.out.println(segmentoT.getNossoNumero() + " - " + segmentoT.getValorNominalTitulo());
            }

            System.out.println("---- SANTANDER ----");
            ProcessamentoRetorno santander = new RetornoBancoBrasil(new File("src/main/java/Santander.TXT"));
            santander.processar();
            for (SegmentoT segmentoT : santander.getSegmentos()) {
                System.out.println(segmentoT.getNossoNumero() + " - " + segmentoT.getValorNominalTitulo());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
