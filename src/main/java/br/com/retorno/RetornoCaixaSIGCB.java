/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.retorno;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author ricardo
 */
public class RetornoCaixaSIGCB extends ProcessamentoRetorno {

    public RetornoCaixaSIGCB(File arquivoRetorno) {
        super(arquivoRetorno);
    }

    @Override
    public void implementacao(BufferedReader br, File arquivoRetorno) throws FileNotFoundException, IOException {
        String str = "";
        int cont = 0;
        SegmentoT st = new SegmentoT();
        while ((str = br.readLine()) != null) {
            cont++;
            if (cont >= 2) {
                switch (String.valueOf(str.charAt(13))) {
                    case "T":
                        st.setNossoNumero(str.substring(46, 56));
                        break;
                    case "U":
                        st.setValorNominalTitulo(str.substring(77, 92));
                        st.setDtVenciTitulo(str.substring(137, 145));
                        break;
                }
                if (st.getNossoNumero() != null && st.getValorNominalTitulo() != null) {
                    super.getSegmentos().add(st);
                    st = new SegmentoT();
                }
            }
        }
    }

}
