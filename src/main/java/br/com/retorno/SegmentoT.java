/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.retorno;

import java.math.BigDecimal;

/**
 *
 * @author ricardo
 */
public class SegmentoT {

    private String codigoBanco;
    private String numLote;
    private String tipoRegistro;
    private String sequencialReg;
    private String segmentoRegistro;
    private String reservadoBancoI;
    private String codigoMovimentacao;
    private String agenciaCedente;
    private String digitoAgencia;
    private String numeroContaCorrente;
    private String digitoVerificador;
    private String reservadoBancoII;
    private String nossoNumero;
    private String codigoCarteria;
    private String numDocCobranca;
    private String dtVenciTitulo;
    private BigDecimal valorNominalTitulo;
    private String numBancoCobrador;
    private String agenciaCobradora;
    private String digitoAgenciaCedente;
    private String identiTituloEmpresa;
    private String codigoMoeda;
    private String tipoIsencaoSacado;
    private String numInscricaoSacado;
    private String nomeSacado;
    private String contaCobraca;
    private BigDecimal valorTarifaCustos;
    private String identificacaoRejeicaoTarifasCustos;
    private String reservadoBancoIII;

    public SegmentoT(String codigoBanco, String numLote, String tipoRegistro, String sequencialReg, String segmentoRegistro, String reservadoBancoI, String codigoMovimentacao, String agenciaCedente, String digitoAgencia, String numeroContaCorrente, String digitoVerificador, String reservadoBancoII, String nossoNumero, String codigoCarteria, String numDocCobranca, String dtVenciTitulo, String valorNominalTitulo, String numBancoCobrador, String agenciaCobradora, String digitoAgenciaCedente, String identiTituloEmpresa, String codigoMoeda, String tipoIsencaoSacado, String numInscricaoSacado, String nomeSacado, String contaCobraca, String valorTarifaCustos, String identificacaoRejeicaoTarifasCustos, String reservadoBancoIII) {
        this.codigoBanco = codigoBanco;
        this.numLote = numLote;
        this.tipoRegistro = tipoRegistro;
        this.sequencialReg = sequencialReg;
        this.segmentoRegistro = segmentoRegistro;
        this.reservadoBancoI = reservadoBancoI;
        this.codigoMovimentacao = codigoMovimentacao;
        this.agenciaCedente = agenciaCedente;
        this.digitoAgencia = digitoAgencia;
        this.numeroContaCorrente = numeroContaCorrente;
        this.digitoVerificador = digitoVerificador;
        this.reservadoBancoII = reservadoBancoII;
        this.nossoNumero = nossoNumero;
        this.codigoCarteria = codigoCarteria;
        this.numDocCobranca = numDocCobranca;
        this.dtVenciTitulo = dtVenciTitulo;

        String valorNominalTratado = valorNominalTitulo.substring(0, valorNominalTitulo.length() - 2);
        valorNominalTratado += ".";
        valorNominalTratado += valorNominalTitulo.substring(valorNominalTitulo.length() - 2, valorNominalTitulo.length());
        this.valorNominalTitulo = new BigDecimal(valorNominalTratado);

        this.numBancoCobrador = numBancoCobrador;
        this.agenciaCobradora = agenciaCobradora;
        this.digitoAgenciaCedente = digitoAgenciaCedente;
        this.identiTituloEmpresa = identiTituloEmpresa;
        this.codigoMoeda = codigoMoeda;
        this.tipoIsencaoSacado = tipoIsencaoSacado;
        this.numInscricaoSacado = numInscricaoSacado;
        this.nomeSacado = nomeSacado;
        this.contaCobraca = contaCobraca;
        String valorTarifaTratado = "0";
        if (valorTarifaCustos.length() > 0) {
            valorTarifaTratado = valorTarifaCustos.substring(0, valorTarifaCustos.length() - 2);
            valorTarifaTratado += ".";
            valorTarifaTratado += valorTarifaCustos.substring(valorTarifaCustos.length() - 2, valorTarifaCustos.length());
        }
        this.valorTarifaCustos = new BigDecimal(valorTarifaTratado);
        this.identificacaoRejeicaoTarifasCustos = identificacaoRejeicaoTarifasCustos;
        this.reservadoBancoIII = reservadoBancoIII;
    }

    public SegmentoT() {
    }

    public void setNossoNumero(String nossoNumero) {
        this.nossoNumero = nossoNumero;
    }

    public void setValorNominalTitulo(String valorNominalTitulo) {
        String valorNominalTratado = valorNominalTitulo.substring(0, valorNominalTitulo.length() - 2);
        valorNominalTratado += ".";
        valorNominalTratado += valorNominalTitulo.substring(valorNominalTitulo.length() - 2, valorNominalTitulo.length());
        this.valorNominalTitulo = new BigDecimal(valorNominalTratado);
    }

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setDtVenciTitulo(String dtVenciTitulo) {
        this.dtVenciTitulo = dtVenciTitulo;
    }

    public String getNumLote() {
        return numLote;
    }

    public String getTipoRegistro() {
        return tipoRegistro;
    }

    public String getSequencialReg() {
        return sequencialReg;
    }

    public String getSegmentoRegistro() {
        return segmentoRegistro;
    }

    public String getReservadoBancoI() {
        return reservadoBancoI;
    }

    public String getCodigoMovimentacao() {
        return codigoMovimentacao;
    }

    public String getAgenciaCedente() {
        return agenciaCedente;
    }

    public String getDigitoAgencia() {
        return digitoAgencia;
    }

    public String getNumeroContaCorrente() {
        return numeroContaCorrente;
    }

    public String getDigitoVerificador() {
        return digitoVerificador;
    }

    public String getReservadoBancoII() {
        return reservadoBancoII;
    }

    public String getNossoNumero() {
        return nossoNumero;
    }

    public String getCodigoCarteria() {
        return codigoCarteria;
    }

    public String getNumDocCobranca() {
        return numDocCobranca;
    }

    public String getDtVenciTitulo() {
        return dtVenciTitulo;
    }

    public BigDecimal getValorNominalTitulo() {
        return valorNominalTitulo;
    }

    public String getNumBancoCobrador() {
        return numBancoCobrador;
    }

    public String getAgenciaCobradora() {
        return agenciaCobradora;
    }

    public String getDigitoAgenciaCedente() {
        return digitoAgenciaCedente;
    }

    public String getIdentiTituloEmpresa() {
        return identiTituloEmpresa;
    }

    public String getCodigoMoeda() {
        return codigoMoeda;
    }

    public String getTipoIsencaoSacado() {
        return tipoIsencaoSacado;
    }

    public String getNumInscricaoSacado() {
        return numInscricaoSacado;
    }

    public String getNomeSacado() {
        return nomeSacado;
    }

    public String getContaCobraca() {
        return contaCobraca;
    }

    public BigDecimal getValorTarifaCustos() {
        return valorTarifaCustos;
    }

    public String getIdentificacaoRejeicaoTarifasCustos() {
        return identificacaoRejeicaoTarifasCustos;
    }

    public String getReservadoBancoIII() {
        return reservadoBancoIII;
    }

}
